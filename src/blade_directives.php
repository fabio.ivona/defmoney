<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 11:50
	 */
	
	\Blade::directive('money', function($expression){
		$expression = \DefStudio\Money\Money::format($expression);
		return "<?php echo '$expression' ?>";
	});
<?php
	/**
	 * Powered by DeF Studio (report@defstudio.it)
	 * Date: 25/02/2019
	 * Time: 12:27
	 */
	
	namespace DefStudio\Money;
	
	
	class Money{
		
		public static function format($expression, $decimals=2){
			return '&euro;&nbsp;'.number_format($expression, $decimals);
		}
		
	}